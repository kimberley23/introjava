/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week1_3;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class WeightUnitsSolver {
    /**
     * main to be used for testing purposes
     * @param args 
     */
    public static void main(String[] args) {
        WeightUnitsSolver wus = new WeightUnitsSolver();
        wus.convertFromGrams(1000);
        System.out.println(wus.convertFromGrams(1000));
    }
    
    /**
     * will return the number of Pounds, Ounces and Grams represented by this quantity of grams, 
     * encapsulated in a BritishWeightUnits object.
     * @param grams
     * @return a BritishWeightUnits instance
     * @throws IllegalArgumentException when the Grams quantity is 
     */
    public BritishWeightUnits convertFromGrams(int inputGrams) {
        // Initiate variables.
        int pounds = 0;
        int ounces = 0;
        int grams = 0;
        int restpounds = 0;

        // Raise exception when inputGrams is below 0.
        if (inputGrams <= 0) {
            throw new IllegalArgumentException("Error: amount of grams should be above 0. Given: "
                    + inputGrams);
        }

        // Calculate amount of pounds, ounces and grams.
        pounds = inputGrams / 454;
        restpounds = inputGrams % 454;
        ounces = restpounds /28;
        grams = restpounds % 28;

        return new BritishWeightUnits(pounds, ounces, grams);
    }
}
