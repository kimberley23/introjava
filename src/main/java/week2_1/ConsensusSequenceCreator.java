/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week2_1;

import com.sun.deploy.util.StringUtils;

import java.sql.Array;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;


/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class ConsensusSequenceCreator {
    /**
     * testing main.
     * @param args 
     */
    public static void main(String[] args) {
        String[] sequences = new String[4];
        sequences[0] = "GAAT";
        sequences[1] = "GAAA";
        sequences[2] = "GATT";
        sequences[3] = "GAAC";

        ConsensusSequenceCreator csc = new ConsensusSequenceCreator();
        String consensus = csc.createConsensus(sequences, true);
        //consensus should equal "GAWH"
        consensus = csc.createConsensus(sequences, false);
        //consensus should equal "GA[A/T][A/T/C]"
    }

    /**
     * creates a consensus sequence from the given array of sequences.
     * @param sequences the sequences to scan for consensus
     * @param iupac flag to indicate IUPAC (true) or bracket notation (false)
     * @return consensus the consensus sequence
     */
    public String createConsensus(String[] sequences, boolean iupac) {
        /** Create HashMap with all iupac codes and bracket notations
         *  Set up: HashMap<String, String[]>
         *      String[] contains [iupac code, bracket notation]
         */
        HashMap map = new HashMap<String, String[]>()
        {{
            put("A", new String[] {"A", "A"});
            put("C", new String[] {"C", "C"});
            put("G", new String[] {"G", "G"});
            put("T", new String[] {"T", "T"});
            put("U", new String[] {"U", "U"});
            put("AT", new String[] {"W", "[A/T]"});
            put("CG", new String[] {"S", "[C/G]"});
            put("AC", new String[] {"M", "[A/C]"});
            put("GT", new String[] {"K", "[G/T]"});
            put("AG", new String[] {"R", "[A/G]"});
            put("CT", new String[] {"Y", "[C/T]"});
            put("CGT", new String[] {"B", "[C/G/T]"});
            put("AGT", new String[] {"D", "[A/G/T]"});
            put("ACT", new String[] {"H", "[A/C/T]"});
            put("ACGT", new String[] {"N", "[A/C/G/T]"});
        }};

        // Create StringBuilder to merge all strings
        StringBuilder concensus = new StringBuilder();
        // Create set for each nucleotide location
        for (int i=0; i < sequences[0].length(); i++){
            // Use treeset to sort set
            TreeSet set = new TreeSet();
            for (int j = 0; j < sequences.length; j++){
                // Add nucleotide to set if it didn't exist
                set.add(Character.toString(sequences[j].charAt(i)));
            }
            // Create string from set with StringUtils.join
            String setToString = StringUtils.join(set, "");

            // Choice between notation system (iupac or bracket)
            if (iupac){
                // Add notation to StringBuilder concensus
                concensus.append(((String[]) map.get(setToString))[0]);
            }
            else{
                // Add notation to StringBuilder concensus
                concensus.append(((String[]) map.get(setToString))[1]);
            }
        }
        // Return concensus sequence (in String format)
        return concensus.toString();
        }
}
