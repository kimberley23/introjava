/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week4_1;

import java.util.*;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Protein implements Comparable<Protein> {
    private final String name;
    private final String accession;
    private final String aminoAcidSequence;
    private GOannotation goAnnotation;
    private double weight;
    // HashmMap containing all amino acids with corresponding molecular weights
    private final HashMap<Character, Double> aaWeights = new HashMap<Character, Double>(){{
        put('I', 131.1736);
        put('L', 131.1736);
        put('K', 146.1882);
        put('M', 149.2124);
        put('F', 165.1900);
        put('T', 119.1197);
        put('W', 204.2262);
        put('V', 117.1469);
        put('R', 174.2017);
        put('H', 155.1552);
        put('A',  89.0935);
        put('N', 132.1184);
        put('D', 133.1032);
        put('C', 121.1590);
        put('E', 146.1451);
        put('Q', 147.1299);
        put('G',  75.0669);
        put('P', 115.1310);
        put('S', 105.0930);
        put('Y', 181.1894);
    }};

    /**
     * constructs without GO annotation;
     * @param name name of protein
     * @param accession accession number of protein
     * @param aminoAcidSequence amino acid sequence of protein
     */
    public Protein(String name, String accession, String aminoAcidSequence) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = setAminoAcidSequence(aminoAcidSequence);
    }

    /**
     * construicts with main properties.
     * @param name name of protein
     * @param accession accession number of protein
     * @param aminoAcidSequence amino acid sequence of protein
     * @param goAnnotation Go Annotation object for protein
     */
    public Protein(String name, String accession, String aminoAcidSequence, GOannotation goAnnotation) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = setAminoAcidSequence(aminoAcidSequence);
        this.goAnnotation = goAnnotation;
    }

    @Override
    public int compareTo(Protein other) {
        // Default sorting type (has to stand here so it is taken by default)
        // SortingType.PROTEIN_NAME -> sort by protein name.
        String compareName = other.getName().toLowerCase();
        return this.getName().toLowerCase().compareTo(compareName);
    }

    
    /**
     * provides a range of possible sorters, based on the type that is requested.
     * @param type type used for sorting
     * @return proteinSorter
     */
    public static Comparator<Protein> getSorter(SortingType type) {
        if (type == null){
            throw new IllegalArgumentException("The input of sorting type is not valid. Please enter a valid " +
                    "sorting type");
        }
        else {
            switch (type) {
                case PROTEIN_NAME:
                default:
                    return new Comparator<Protein>() {
                        public int compare(Protein protein1, Protein protein2) {
                            return protein1.getName().toLowerCase().compareTo(protein2.getName().toLowerCase());
                        }
                    };
                case ACCESSION_NUMBER:
                    return new Comparator<Protein>() {
                        public int compare(Protein protein1, Protein protein2) {
                            return protein1.getAccession().toLowerCase().compareTo(
                                    protein2.getAccession().toLowerCase());
                        }
                    };
                case GO_ANNOTATION:
                    return new Comparator<Protein>() {
                        public int compare(Protein protein1, Protein protein2) {
                            int compareResult;
                            compareResult = protein1.getGoAnnotation().getBiologicalProcess().compareTo(
                                    protein2.getGoAnnotation().getBiologicalProcess());
                            if (compareResult == 0) {
                                compareResult = protein1.getGoAnnotation().getCellularComponent().compareTo(
                                        protein2.getGoAnnotation().getCellularComponent());
                                if (compareResult == 0) {
                                    compareResult = protein1.getGoAnnotation().getMolecularFunction().compareTo(
                                            protein2.getGoAnnotation().getMolecularFunction());
                                }
                            }
                            return compareResult;
                        }
                    };
                case PROTEIN_WEIGHT:
                    return new Comparator<Protein>() {
                        public int compare(Protein protein1, Protein protein2) {
                            return Double.compare(protein2.getWeight(), protein1.getWeight());
                        }
                    };
            }
        }
    }


    /** ***************************************************************************
     *                                  Getters
     ******************************************************************************/

    /**
     * @return name name of protein
     */
    public String getName() {return name;}

    /**
     * @return accession accession number of protein
     */
    public String getAccession() {return accession;}

    /**
     * @return aminoAcidSequence amino acid sequence of protein
     */
    public String getAminoAcidSequence() {return aminoAcidSequence;}

    /**
     * @return GO annotation Go Annotation object of protein
     */
    public GOannotation getGoAnnotation() {return goAnnotation;}


    /**
     * @return weight protein weight
     */

    public double getWeight() {
        if (weight == 0){
            double weightTotal = 0;
            for (int i = 0; i < aminoAcidSequence.length(); i++){
                weightTotal += aaWeights.get(aminoAcidSequence.charAt(i));
            }
            weight = weightTotal;
        }
        return weight; }

    /** ***************************************************************************
     *                                  String representation
     ******************************************************************************/
    @Override
    public String toString() {
        return "Protein{" + "name=" + name + ", accession=" + accession + ", aminoAcidSequence=" +
                aminoAcidSequence + "}";
    }


    /** ***************************************************************************
     *                           Checks for input (setters)
     ******************************************************************************/

    /**
     * @return amino acid sequence
     */
    public String setAminoAcidSequence(String aminoAcidSequence) {
        TreeSet<Character> validAminoAcids = new TreeSet<Character>(Arrays.asList('A', 'R', 'N', 'D', 'C', 'E', 'Q',
                'Z', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V'));
        TreeSet<Character> aminoAcids = new TreeSet<Character>();
        for(char c : aminoAcidSequence.toUpperCase().toCharArray()) {
            aminoAcids.add(c);
        }
        if (validAminoAcids.containsAll(aminoAcids)){
            return aminoAcidSequence.toUpperCase();
        }
        else{
            throw new IllegalArgumentException("Error: some amino acids aren't valid. Please give a sequence with " +
                    "correct amino acids.");
        }

    }
}


