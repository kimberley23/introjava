/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Animal {
    protected String name;
    protected double maxSpeed;
    protected int maxAge;
    protected int age;

    public Animal(String startName, int startAge){
        name = startName;
        age = startAge;
    }

    /**
     * returns the name of the animal
     * @return name the species name
     */
    public String getName() {
        return this.name;
    }

    /**
     * returns the age of the animal
     * @return age the species age
     */
    public int getAge() {
        return this.age;
    }

    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public String getMovementType() {
        return null;
    }
    

    /**
     * returns the speed of this animal
     * @return speed the speed of this animal
     */
    public double getSpeed() {
        double speed = this.maxSpeed * (0.5 + (0.5 * ((this.maxAge - this.age) / (double)(this.maxAge))));
        double roundOff = Math.round(speed * 10.0) / 10.0;
        return roundOff;
    }

    /**
     * returns string of info about animal
     * @return string info of this animal
     */
    public String printInfo() {
        if(this.name.startsWith("A") || this.name.startsWith("E") || this.name.startsWith("I") || this.name.startsWith("O") || this.name.startsWith("U")){
            String printInfo = "An " + this.name + " of age " + this.age + " moving in " + this.getMovementType() + " at " + this.getSpeed() + " km/h";
            return printInfo;
        }
        if(this.age > this.maxAge){
            String printInfo = "Error: maximum age of " + this.name + " is " + this.maxAge + " years. Please give new values";
            return printInfo;
        }
        else {
            String printInfo = "A " + this.name + " of age " + this.age + " moving in " + this.getMovementType() + " at " + this.getSpeed() + " km/h";
            return printInfo;
        }
    }


}
