/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

import java.util.*;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class AnimalSimulator {
    public static void main(String[] args) {
        AnimalSimulator anSim = new AnimalSimulator();
        anSim.getSupportedAnimals();
        anSim.start(args);
    }

    
    private void start(String[] args) {
        //start processing command line arguments
        if (args.length == 0 || args[0].equals("help")) {
            System.out.println("Usage: java AnimalSimulator <Species age Species age ...>\n" +
                    "Supported species (in alphabetical order):");
            getSupportedAnimals();
            // Sort of enumerate function from python
            ListIterator<String> it = getSupportedAnimals().listIterator();
            while (it.hasNext()) {
                System.out.println(it.nextIndex() + 1 + ": " + it.next());
            }
        }
        if (args.length >= 2) {
            List<Object> addedAnimals;
            for (int i = 0; i < args.length; i += 2) {
                String str = args[i];
                switch (str) {
                    case "Elephant":
                    case "elephant":
                        Elephant elephant = new Elephant("Elephant", Integer.parseInt(args[i + 1]));
                        System.out.println(elephant.printInfo());
                        break;
                    case "horse":
                    case "Horse":
                        Horse horse = new Horse("Horse", Integer.parseInt(args[i + 1]));
                        System.out.println(horse.printInfo());
                        break;
                    // System.out.println("A Horse of age 21 moving in gallop at 73.1 km/h");
                    case "Mouse":
                    case "mouse":
                    case "Housemouse":
                    case "housemouse":
                    case "House mouse":
                        HouseMouse houseMouse = new HouseMouse("Mouse", Integer.parseInt(args[i + 1]));
                        System.out.println(houseMouse.printInfo());
                        break;
                    case "Tortoise":
                    case "tortoise":
                        Tortoise tortoise = new Tortoise("Tortoise", Integer.parseInt(args[i + 1]));
                        System.out.println(tortoise.printInfo());
                        break;
                    default:
                        System.out.println("Error: animal species " + str + " is not known. run with single parameter \"help\" to get a listing of available species. Please give new values");
                        break;
                }
            }
        }
    }

    /**
     * returns all supported animals as List, alhabetically ordered
     * @return supportedAnimals the supported animals
     */
    public List<String> getSupportedAnimals() {
        // Create list with all supported animals
        List<String> allAnimals = Arrays.asList("Horse", "Elephant", "Mouse", "Tortoise");
        // Sort list in alphabetic order
        Collections.sort(allAnimals);
        return allAnimals;
    }

}
