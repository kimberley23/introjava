/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class HouseMouse extends Animal{
    public HouseMouse (String startName,
                     int startAge){
        super(startName, startAge);
        maxSpeed = 21;
        maxAge = 13;
    }

    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public String getMovementType() {
        return "scurry";
    }
}
