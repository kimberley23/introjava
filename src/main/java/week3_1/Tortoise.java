/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Tortoise extends Animal {
    public Tortoise (String startName,
                     int startAge){
        super(startName, startAge);
        maxSpeed = 0.3;
        maxAge = 190;
    }

    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public String getMovementType() {
        return "crawl";
    }
}
