/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package week3_1;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Horse extends Animal{
    public Horse (String startName,
                     int startAge){
        super(startName, startAge);
        maxSpeed = 88;
        maxAge = 62;
    }

    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public String getMovementType() {
        return "gallop";
    }

}
