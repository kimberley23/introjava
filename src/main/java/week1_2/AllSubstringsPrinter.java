/*
 * Copyright (c) 2014 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package week1_2;

import java.util.Collections;

/**
 *
 * @author michiel
 */
public class AllSubstringsPrinter {
    /**
     * main method serves development purposes only.
     * @param args the args
     */
    public static void main(final String[] args) {
        AllSubstringsPrinter asp = new AllSubstringsPrinter();
        asp.printAllSubstrings("GATCG", true, false); //should print left truncated, left aligned
    }

    /**
     * will print all possible substrings according to arguments.
     * @param stringToSubstring the string to substring
     * @param leftTruncated flag to indicate whether the substrings should be truncated from the left (or the right)
     * @param leftAligned flag to indicate whether the substrings should be printed left-aligned (or right-aligned)
     */
    public void printAllSubstrings(
            final String stringToSubstring,
            final boolean leftTruncated,
            final boolean leftAligned) {

        if (leftTruncated && leftAligned){
            for (int i = 0; i < stringToSubstring.length(); i++) {
                System.out.println(stringToSubstring.substring(i, stringToSubstring.length()));
            }
        }
        if (!leftTruncated && leftAligned){
            for (int i = 0; i < stringToSubstring.length(); i++) {
                System.out.println(stringToSubstring.substring(0, stringToSubstring.length() - i));
            }
        }
        if (leftTruncated && !leftAligned) {
            for (int i = 0; i < stringToSubstring.length(); i++) {
                System.out.println(String.join("", Collections.nCopies(i, " ")) + stringToSubstring.substring(i, stringToSubstring.length()));
            }
        }
        if (!leftTruncated && !leftAligned){
            for (int i = 0; i < stringToSubstring.length(); i++) {
                System.out.println(String.join("", Collections.nCopies(i, " ")) + stringToSubstring.substring(0, stringToSubstring.length() - i));
            }
        }
    }
}
